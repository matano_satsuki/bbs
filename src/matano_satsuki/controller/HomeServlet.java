package matano_satsuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import matano_satsuki.beans.Category;
import matano_satsuki.beans.Comment;
import matano_satsuki.beans.Post;
import matano_satsuki.beans.User;
import matano_satsuki.beans.UserComment;
import matano_satsuki.beans.UserPost;
import matano_satsuki.service.CommentService;
import matano_satsuki.service.PostService;
import matano_satsuki.service.UserService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<UserComment> comment = new CommentService().getComment();
		List<Category> categorys = new PostService().getCategory();

		String start_date = "2016-01-01"; //デフォルト
		String end_date = "2100-12-31";
		String select_category = "";

		if(StringUtils.isEmpty(request.getParameter("start_date")) == false ){
			start_date = request.getParameter("start_date");
			request.setAttribute("start_date", start_date);
		}
		if(StringUtils.isEmpty(request.getParameter("end_date")) == false){
			end_date = request.getParameter("end_date");
			request.setAttribute("end_date", end_date);
		}
		if(StringUtils.isEmpty(request.getParameter("s_category")) == false){
			select_category = request.getParameter("s_category");
			request.setAttribute("select", select_category);
		}
		List<UserPost> post =
				new PostService().getDatePost(start_date, end_date, select_category);

		User loginUser = (User) session.getAttribute("loginUser");
		User user = new UserService().getUser(loginUser.getId());

		request.setAttribute("loginUser", user);
		request.setAttribute("posts", post);
		request.setAttribute("categorys", categorys);
		request.setAttribute("comments", comment);
		request.getRequestDispatcher("./home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8");
		String start_date = "2016-01-01"; //デフォルト
		String end_date = "2100-12-31";
		String select_category = "";

		if(StringUtils.isEmpty(request.getParameter("start_date")) == false ){
			start_date = request.getParameter("start_date");
			request.setAttribute("start_date", start_date);
		}
		if(StringUtils.isEmpty(request.getParameter("end_date")) == false){
			end_date = request.getParameter("end_date");
			request.setAttribute("end_date", end_date);
		}
		if(StringUtils.isEmpty(request.getParameter("s_category")) == false){
			select_category = request.getParameter("s_category");
			request.setAttribute("select", select_category);
		}

		if(request.getParameter("text") != null){
			List<String> messages = new ArrayList<String>();
			HttpSession session = request.getSession();
			Comment entryComment = getEntryComment(request);
			request.setAttribute("entryComment", entryComment);

			if(isValid(entryComment, messages) == true){

				User user = (User) session.getAttribute("loginUser");

				Comment comment = new Comment();
				comment.setText(request.getParameter("text"));
				comment.setUser_id(user.getId());
				comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));

				new CommentService().register(comment);
				new CommentService().update(comment);

				response.sendRedirect("./");
			} else {
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("./");

			}
		}

		String deleteParam = request.getParameter("delete");

		if (deleteParam != null && !"".equals(deleteParam)){
			if ("post_delete".equals(deleteParam)) {
				Post post = new Post();
				post.setId(Integer.parseInt(request.getParameter("id")));
				new PostService().deletePost(post);
				// コメントは削除されない
			} else if ("cmnt_delete".equals(deleteParam)) {
				Comment cmnt = new Comment();
				cmnt.setId(Integer.parseInt(request.getParameter("id")));
				new CommentService().delete(cmnt.getId());
			}
			response.sendRedirect("./");
		}
	}

	private Comment getEntryComment(HttpServletRequest request)
			throws IOException, ServletException{

		Comment entryComment = new Comment();

		entryComment.setText(request.getParameter("text"));
		return entryComment;
	}

	private boolean isValid(Comment entryComment, List<String> messages) {
		String text = entryComment.getText();

		if(StringUtils.isEmpty(text) == true){
			messages.add("コメントが入力されていません。");
		}
		if (text.length() > 500){
			messages.add("コメントは500文字以下で入力してください。");
		}

		if(messages.size() == 0){
			return true;
		} else {
			return false;
		}
	}
}