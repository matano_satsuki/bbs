package matano_satsuki.service;

import static matano_satsuki.utils.CloseableUtil.*;
import static matano_satsuki.utils.DBUtil.*;

import java.sql.Connection;

import matano_satsuki.beans.User;
import matano_satsuki.dao.UserDao;
import matano_satsuki.utils.CipherUtil;

public class LoginService {
	
	public User login(String login_id, String password){
		Connection connection = null;
		try{
			connection = getConnection();
			
			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao
					.getUser(connection, login_id, encPassword);
			
			commit(connection);
			
			return user;
		} catch (RuntimeException e){
			rollback(connection);
			throw e;
		} catch (Error e){
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
