package matano_satsuki.service;

import static matano_satsuki.utils.CloseableUtil.*;
import static matano_satsuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import matano_satsuki.beans.Branch;
import matano_satsuki.dao.BranchDao;

public class BranchService {
	public List<Branch> getBranch() {
		
		List<Branch> ret = new ArrayList<>();

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> branch = branchDao.getBranch(connection);
			ret.addAll(branch);
			
			commit(connection);
			
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
