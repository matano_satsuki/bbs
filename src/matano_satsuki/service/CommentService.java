package matano_satsuki.service;

import static matano_satsuki.utils.CloseableUtil.*;
import static matano_satsuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import matano_satsuki.beans.Comment;
import matano_satsuki.beans.UserComment;
import matano_satsuki.dao.CommentDao;
import matano_satsuki.dao.UserCommentDao;

public class CommentService {

	public void register(Comment comment) {
		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserComment> getComment(){

		Connection connection = null;
		try{
			connection = getConnection();

			UserCommentDao commentDao = new UserCommentDao();
			List<UserComment> ret = commentDao.getUserComment(connection, LIMIT_NUM);

			commit(connection);
			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e){
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(Comment comment) {
		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.update(connection, comment);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int delete_id) {
		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.comment_delete(connection, delete_id);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
