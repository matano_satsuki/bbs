package matano_satsuki.dao;

import static matano_satsuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import matano_satsuki.beans.Comment;
import matano_satsuki.exception.NoRowsUpdatedRuntimeException;
import matano_satsuki.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comment (");
			sql.append(" user_id");
			sql.append(", message_id");
			sql.append(", text");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(") VALUE (");
			sql.append("?"); //user_id
			sql.append(",?"); //m_id
			sql.append(",?"); //text
			sql.append(",CURRENT_TIMESTAMP"); //insert_d
			sql.append(",CURRENT_TIMESTAMP"); //update_d
			sql.append(");");


			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUser_id());
			ps.setInt(2, comment.getMessage_id());
			ps.setString(3, comment.getText());

			ps.executeUpdate();

		} catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			close(ps);
		}
	}

	public void update(Connection connection, Comment comment){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE postmessage SET ");
			sql.append(" insert_date = insert_date ,");
			sql.append(" update_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getMessage_id());

			ps.executeUpdate();

		} catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			close(ps);
		}
	}

	/**
	 * comment_delete
	 * @param connection
	 * @param commentID
	 */
	public void comment_delete(Connection connection, int id){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comment");
			sql.append(" WHERE");
			sql.append(" id = ?");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			close(ps);
		}
	}
}