package matano_satsuki.dao;

import static matano_satsuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import matano_satsuki.beans.Category;
import matano_satsuki.beans.UserPost;
import matano_satsuki.exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPost(Connection connection, int num) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM postmessage ");
			sql.append("ORDER BY update_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<UserPost> toUserPostList(ResultSet rs)
		throws SQLException{

		List<UserPost> ret = new ArrayList<UserPost>();
		try{
			while(rs.next()){
				int message_id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				int branch_id = rs.getInt("branch_id");
				int department_id = rs.getInt("department_id");
				String name = rs.getString("name");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp insertDate = rs.getTimestamp("insert_date");

				UserPost post = new UserPost();
				post.setMessage_id(message_id);
				post.setUser_id(user_id);
				post.setBranch_id(branch_id);
				post.setDepartment_id(department_id);
				post.setName(name);
				post.setTitle(title);
				post.setText(text);
				post.setCategory(category);
				post.setInsert_date(insertDate);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);

		}
	}

	public List<UserPost> getDatePost(Connection connection, int num,
			String start_date, String end_date, String category) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT")
			.append(" post.id,")
			.append(" post.user_id,")
			.append(" post.title,")
			.append(" post.text,")
			.append(" post.category,")
			.append(" post.insert_date,")
			.append(" user.name,")
			.append(" user.branch_id,")
			.append(" user.department_id")
			.append(" FROM")
			.append(" postmessage AS post,")
			.append(" user")
			.append(" WHERE")
			.append(" post.user_id = user.id")
			.append(" AND")
			.append(" post.insert_date >= ?")
			.append(" AND")
			.append(" post.insert_date <= ?");
			if(category != ""){
				sql.append(" AND post.category = ?");
			}
			sql.append(" ORDER BY post.update_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start_date);
			ps.setString(2, end_date);
			if(!StringUtils.isNullOrEmpty(category)){
				ps.setString(3, category);
			}
//			if(category != ""){
//				ps.setString(3, category);
//			}

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		} catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Category> getCategory(Connection connection) {

		PreparedStatement ps = null;
		try{
			String sql = "SELECT distinct category FROM postmessage";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<Category> ret = toCategoryList(rs);
			return ret;
		} catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<Category> toCategoryList(ResultSet rs)
		throws SQLException{

		List<Category> ret = new ArrayList<Category>();
		try{
			while(rs.next()){
				String category = rs.getString("category");

				Category categorySet = new Category();
				categorySet.setCategory(category);

				ret.add(categorySet);
			}
			return ret;
		} finally {
			close(rs);

		}
	}

}
